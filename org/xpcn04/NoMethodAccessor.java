package org.xpcn04;

import ognl.MethodAccessor;
import ognl.MethodFailedException;

import java.util.Map;

/**
 * Created by xpcn04 on 16/3/17.
 */
public class NoMethodAccessor implements MethodAccessor {
    @Override
    public Object callStaticMethod(Map map, Class aClass, String s, Object[] objects) throws MethodFailedException {
        throw new MethodFailedException("do not run...", "what are you doing?");
    }

    @Override
    public Object callMethod(Map map, Object o, String s, Object[] objects) throws MethodFailedException {
        throw new MethodFailedException("do not run...", "what are you doing?");

    }
}