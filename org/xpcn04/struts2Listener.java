package org.xpcn04;

/**
 * Created by tinshen on 16/3/18.
 */
import ognl.OgnlRuntime;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


/**
 * Created by xpcn04 on 16/3/17.
 */
public class struts2Listener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            OgnlRuntime.setMethodAccessor(Runtime.class, new NoMethodAccessor());
            OgnlRuntime.setMethodAccessor(System.class, new NoMethodAccessor());
            OgnlRuntime.setMethodAccessor(ProcessBuilder.class,new NoMethodAccessor());
            OgnlRuntime.setMethodAccessor(OgnlRuntime.class, new NoMethodAccessor());
            System.out.println("struts2 fix is success !");
        } catch (Exception e) {
            System.out.println("struts2 fix is error !");
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }

    public static void main(String[] args) {

    }
}
